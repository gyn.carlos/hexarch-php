<?php
declare(strict_types=1);

namespace Web;

require_once "../vendor/autoload.php";

use DI\ContainerBuilder;

final class TipoObjeto 
{
    public function somar($a, $b) 
    {
        return $a + $b;
    }
}

final class Negociar 
{
    public function __construct(\Web\TipoObjeto $objValor) 
    {
        $this->tipoObjeto = $objValor;
    }

    public function quantoVale($a, $b)
    {
        return $this->tipoObjeto->somar($a, $b);
    }
}

$builder = new ContainerBuilder();
$builder->addDefinitions([
    Negociar::class => \DI\create('\Web\Negociar')
]);
$container = $builder->build();
$negociar  = $container->get("\Web\Negociar");
echo $negociar->quantoVale(350,100);