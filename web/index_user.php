<?php

use App\Usuario\Application\ControllerFacade;
use App\Usuario\Domain\Entity\Usuario;
use App\Usuario\Infra\Database\File;
use App\Usuario\Infra\Presentation\ViewHTML;
use App\Usuario\Infra\Presentation\VarDump;

ini_set('display_errors',-1);
ini_set('display_errors_startup',-1);
error_reporting(E_ALL);

require_once "../vendor/autoload.php";

try {

    // infra
    $repository = new File();

    // application
    $manager    = new ControllerFacade($repository);

    // domain
    $domain     = new Usuario("1","carlos augusto");

    $newUser    = $manager->save($domain);
 
    // presenter var_dump
    $presenter = new VarDump($newUser);

    $presenter->render((array) $newUser);

    // presenter template
    (new ViewHTML())->render($newUser);

} catch(Exception $e) {
    echo $e->getMessage();
}
