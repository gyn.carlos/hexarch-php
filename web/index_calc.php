<?php

ini_set('display_errors',-1);
ini_set('display_errors_startup',-1);
error_reporting(E_ALL);

require_once "../vendor/autoload.php";

use App\Calculadora\Application\Handle;
use App\Calculadora\Domain\CalcService;
use App\Calculadora\Domain\Calc;

use App\Calculadora\Infrastructure\Database\Repository;

try {

    // infra layer
    $repository     = new Repository;    

    // domain service
    $calcService    = new CalcService($repository);

    // application
    $calcController = new Handle($calcService);
    
    // domain
    $domainCalc     = new Calc((int)$_REQUEST['a'], (int) $_REQUEST['b']);

    $calc = $calcController->sum($domainCalc);

    echo $calc;

} catch(Exception $e) {
    echo $e->getMessage();
}


//https://www.youtube.com/watch?v=x9wSiw5u7Yk&list=PLBD8to5dJhvyr07t03AjYYQ_8LNHrQKF4&index=3&ab_channel=YiiAcademy

