# About

Projeto de uma arquitetura que leve em consideração os princípios de ports-and-adapters (hexarch) com Domain Driven Design (DDD) em PHP com objetivo de migrar um código legado.

# Requisitos

1. Docker/Docker-compose;
1. PHP 7.4;
1. Opcional Swoole;

# Boas práticas a ser utilizadas

1. declare(strict_types=1);
1. PSR 2 AND 12
1. final class
1. composer
1. container D.I
1. Tdd

# Iniciando

1. Suba o docker com docker-compose
1. composer install
1. Execute http://localhost:8020/web/index_user.php caso queira o navegador
1. Execute php api/index.php (Swoole + Hyperf) caso queira uma web api.

> O arquivo web/index_user.php é um exemplo de implementação.


# Referências

1. https://github.com/hirannor/spring-boot-hexagonal-architecture/tree/master/infrastructure;

1. https://blog.octo.com/en/hexagonal-architecture-three-principles-and-an-implementation-example/;

1. Muito bom - https://cohesivebytes.com/2016/12/21/hexagonal-architecture/;

1. Exemplos de implementação https://github.com/oumarkonate/hexagonal-architecture;

1. https://github.com/dersonsena/clean-arch-pokemon;

1. https://alistair.cockburn.us/hexagonal-architecture/;

1. https://github.com/victor-falcon/php-ddd;

1. https://stackoverflow.com/questions/66971751/layers-in-hexagonal-architecture
