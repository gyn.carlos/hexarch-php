<?php

namespace App\Calculadora\Domain;

/**
 * Services implements port of domain
 */

class CalcService implements CalcServiceInterface
{
    private $repository;

    public function __construct (CalcRepositoryInterface $repository) 
    {
        $this->repository = $repository;
    }

    public function sum(Calc $calc)
    {        
        return $calc->getValueA() + $calc->getValueB();
    }
}