<?php

namespace App\Calculadora\Domain;

/**
 * Interface (Port) implemented by Infrastructure's adapter.
 * 
 * Domain uses this port to call infrastructure's adapter.
 */

interface CalcRepositoryInterface 
{
    public function save($value);
}