<?php

namespace App\Calculadora\Domain;

use DomainException;

/**
 * Entity
 */

class Calc
{
    private $a;
    private $b;

    public function __construct ($a,$b) 
    {
        if (empty($a) || empty($b)) {
            throw new DomainException (
                "Operadores em branco"
            );
        }

        $this->a = $a;
        $this->b = $b;
    }

    public function getValueA()
     {
        return $this->a;
     }

     public function getValueB()
     {
        return $this->b;
     }
}
