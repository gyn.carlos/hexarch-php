<?php

namespace App\Calculadora\Domain;

use App\Calculadora\Domain\Calc;

/**
 * Interface (Port) use by application.
 */

interface CalcServiceInterface 
{
    public function sum(Calc $calc);
}