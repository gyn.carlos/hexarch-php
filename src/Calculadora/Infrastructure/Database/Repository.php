<?php

namespace App\Calculadora\Infrastructure\Database;

use App\Calculadora\Domain\CalcRepositoryInterface;

/**
 * Adapter implements Domain port CalcOutputInterface.php
 */

class Repository implements CalcRepositoryInterface
{
    public function save($sum)
    {
        file_put_contents(__DIR__ . "/../../logs/result_calc.txt", $sum . PHP_EOL, FILE_APPEND);
    }
}