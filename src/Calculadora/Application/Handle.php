<?php

namespace App\Calculadora\Application;

use App\Calculadora\Domain\CalcServiceInterface;
use App\Calculadora\Domain\Calc;

class Handle
{
    private $calculadora;

    public function __construct(CalcServiceInterface $calculadora)
    {
        $this->calculadora = $calculadora;    
    }

    public function sum(Calc $calc)
    {
        return $this->calculadora->sum($calc);
    }
}