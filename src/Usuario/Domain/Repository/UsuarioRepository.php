<?php

// port for infraescruture

namespace App\Usuario\Domain\Repository;

use App\Usuario\Domain\Entity\Usuario;

interface UsuarioRepository 
{
    public function save(Usuario $objUsuario);
}