<?php

// entity the domain

namespace App\Usuario\Domain\Entity;

class Usuario 
{
    public function __construct(?int $id, ?string $nome)
    {
        $this->id = $id;
        $this->nome = $nome;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }
}