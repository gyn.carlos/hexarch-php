<?php

// port for application

namespace App\Usuario\Domain;

use App\Usuario\Domain\Entity\Usuario;

interface UsuarioService 
{
    public function save(Usuario $objUsuario);    
}