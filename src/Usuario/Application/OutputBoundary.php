<?php

declare(strict_types=1);

namespace App\Usuario\Application;

use App\Usuario\Domain\Entity\Usuario;

final class OutputBoundary 
{
    private $objUsuario;

    public function __construct(Usuario $objUsuario)
    {
        $this->objUsuario = $objUsuario;
    }

    public function toArray()
    {
        return [
            'id'   => $this->objUsuario->getId(),
            'nome' => $this->objUsuario->getNome()
        ];
    }
}