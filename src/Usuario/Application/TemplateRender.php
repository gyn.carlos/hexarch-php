<?php declare(strict_types=1);

namespace App\Usuario\Application;

interface TemplateRender
{
    public function render(Array $data);
}
