<?php

// direction adapter -> domain

// esse aqui � o usecase https://youtu.be/x9wSiw5u7Yk?t=841
// usercase tamb�m chamado de service

namespace App\Usuario\Application;

use App\Usuario\Domain\Entity\Usuario;
use App\Usuario\Domain\Repository\UsuarioRepository;
use App\Usuario\Domain\UsuarioService;

use App\Usuario\Application\OutputBoundary;

class ControllerFacade implements UsuarioService
{
    public function __construct(UsuarioRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(Usuario $objUsuario)
    {
        // aqui vai a implementa��o do inputBoundary e outputBoundary
        // https://youtu.be/x9wSiw5u7Yk?t=968

        // $this->presenter->render();

        $newUser = $this->repository->save($objUsuario);

        return (new OutputBoundary($newUser))->toArray();
    }
}

// presenter
// https://youtu.be/jGgRZ5SsIYE?t=823

// InputBoundary (Interactor) e retorna OutputBoundary (Presenter);
//https://gist.github.com/lievendoclo/89366bd751514498bb74ffa9ca388d26