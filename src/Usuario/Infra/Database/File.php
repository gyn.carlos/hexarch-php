<?php

// adapter for domain

namespace App\Usuario\Infra\Database;

use App\Usuario\Domain\Entity\Usuario;
use App\Usuario\Domain\Repository\UsuarioRepository;

class File implements UsuarioRepository
{
    public function save(Usuario $objUsuario)
    {
        $dados = $objUsuario->id . ' - '. $objUsuario->nome;

        //file_put_contents("./logs/users.txt", $dados . PHP_EOL, FILE_APPEND);

        return $objUsuario;
    }
}