<?php declare(strict_types=1);

namespace App\Usuario\Infra\Presentation;

use App\Usuario\Application\TemplateRender;

use Twig;

//https://youtu.be/jGgRZ5SsIYE?t=1271
// ExportRegistrationPresenter
// parei aqui. agora falta enteder o presentation e implementar ele.

final class ViewHTML implements TemplateRender 
{
    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../Views/');

        $options = [];
        
        $this->twig = new \Twig\Environment($loader, $options);
    }

    public function render(Array $data)
    {
        $template = $this->twig->load('index.html');

        echo $template->render($data);
    }
}