<?php

declare(strict_types=1);

namespace App\Usuario\Infra\Presentation;

use App\Usuario\Infra\Presentation\Presentation;

final class VarDump implements Presentation
{
    public function render(Array $data)
    {
        var_dump($data);
    }
}