<?php declare(strict_types=1);

namespace App\Usuario\Infra\Presentation;

interface Presentation 
{
    public function render(Array $data);
}