<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Hyperf\Nano\ContainerProxy;
use Hyperf\Nano\Factory\AppFactory;

use App\Usuario\Application\ControllerFacade;
use App\Usuario\Domain\Entity\Usuario;
use App\Usuario\Infra\Database\File;
use App\Usuario\Infra\Presentation\ViewHTML;
use App\Usuario\Infra\Presentation\VarDump;

$app = AppFactory::create("0.0.0.0","9505");

$app->get('/api/', function () {
    // infra
    $repository = new File();

    // application
    $manager    = new ControllerFacade($repository);

    // domain
    $domain     = new Usuario("1","carlos augusto");

    $newUser    = $manager->save($domain);
 
    // presenter JSON n�o implementado
    echo json_encode((array) $newUser);
});

$app->run();